package de.santiv.rbesort.enums;

public enum Strings {
    CONF__DISPLAY_NAME("RBESort"),
    CONF__CONVERT_ENCODED_TO_UNICODE("Convert encoded to unicode"),
    CONF__CONVERT_UNICODE_TO_ENCODED("Convert unicode  to encoded"),
    CONF__KEEP_EMPTY_FIELDS("Keep empty fields"),
    CONF__GROUP_KEYS("Group keys"),
    CONF__CONVERT_UNICODE_TO_ENCODED_UPPER("Convert unicode to encoded upper"),
    CONF__KEY_GROUP_SEPARATOR("Key group separator"),
    CONF__GROUP_LEVEL_DEPTH("Group level depth"),
    CONF__ALIGN_EQUAL_SIGNS("Align equal signs"),
    CONF__GROUP_ALIGN_EQUAL_SIGNS("Group align equal signs"),
    CONF__WRAP_LINE_AT_ESCAPED_NEW_LINE("Wrap lines after escaped new line character"),
    CONF__SPACES_AROUND_EQUAL_SIGNS("Add spaces around equal signs");

    private String description;

    Strings(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


}
