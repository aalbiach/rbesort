package de.santiv.rbesort.configuration;

import java.awt.event.ItemListener;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.ui.components.JBCheckBox;
import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;

import de.santiv.rbesort.enums.Config;
import de.santiv.rbesort.enums.Strings;

// TODO: Komponenten wie in den Eclipse Einstellungen anordnen (und die Restlichen noch konfigurierbar machen)
public class Configuration implements Configurable {


    private JCheckBox keepEmptyFields;
    private JCheckBox convertEncodedToUnicode;
    private JCheckBox convertUnicodeToEncoded;
    private JCheckBox convertUnicodeToEncodedUpper;

    private JCheckBox alignEqualSigns;
    private JCheckBox groupAlignEqualSigns;

    private JCheckBox groupKeys;
    private JTextField groupLevelDepth;
    private JTextField keyGroupSeparator;

    private JCheckBox wrapLineAtEscapedNewLine;
    private JCheckBox spacesAroundEqualsSigns;


    private boolean modified = false;

    @Nls
    @Override
    public String getDisplayName() {
        return Strings.CONF__DISPLAY_NAME.getDescription();
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        initComponents();
        loadAndSetValues();
        initListeners();

        // @formatter:off
        PanelBuilder builder = new PanelBuilder(new FormLayout("pref, 10dlu, 20dlu, pref:grow",
                "p, 5dlu, " +
                        "p, 5dlu, " +
                        "p, 5dlu, " +
                        "p, 5dlu, " +
                        "p, 5dlu, " +
                        "p, 5dlu, " +
                        "p, 5dlu, " +
                        "p, 5dlu, " +
                        "p, 5dlu, " +
                        "p, 5dlu, " +
                        "p, 5dlu, " +
                        "p, 5dlu"));

        CellConstraints cc = new CellConstraints();
        builder.addLabel(Strings.CONF__GROUP_LEVEL_DEPTH.getDescription(), cc.rc(3, 1));
        builder.add(groupLevelDepth, cc.rc(3, 3));
        builder.addLabel(Strings.CONF__KEY_GROUP_SEPARATOR.getDescription(), cc.rc(5, 1));
        builder.add(keyGroupSeparator, cc.rc(5, 3));
        builder.add(alignEqualSigns, cc.rcw(7, 3, 2));
        builder.add(groupAlignEqualSigns, cc.rcw(9, 3, 2));
        builder.add(groupKeys, cc.rcw(11, 3, 2));
        builder.add(keepEmptyFields, cc.rcw(13, 3, 2));
        builder.add(convertEncodedToUnicode, cc.rcw(15, 3, 2));
        builder.add(convertUnicodeToEncoded, cc.rcw(17, 3, 2));
        builder.add(convertUnicodeToEncodedUpper, cc.rcw(19, 3, 2));
        builder.add(wrapLineAtEscapedNewLine, cc.rcw(21, 3, 2));
        builder.add(spacesAroundEqualsSigns, cc.rcw(23, 3, 2));
        // @formatter:on

        return builder.getPanel();
    }

    private void loadAndSetValues() {
        keepEmptyFields.setSelected(Config.loadBooleanValue(Config.KEEP_EMPTY_FIELDS));
        convertEncodedToUnicode.setSelected(Config.loadBooleanValue(Config.CONVERT_ENCODED_TO_UNICODE));
        convertUnicodeToEncoded.setSelected(Config.loadBooleanValue(Config.CONVERT_UNICODE_TO_ENCODED));
        convertUnicodeToEncodedUpper.setSelected(Config.loadBooleanValue(Config.CONVERT_UNICODE_TO_ENCODED_UPPER));
        alignEqualSigns.setSelected(Config.loadBooleanValue(Config.ALIGN_EQUAL_SIGNS));
        groupAlignEqualSigns.setSelected(Config.loadBooleanValue(Config.GROUP_ALIGN_EQUAL_SIGNS));
        groupKeys.setSelected(Config.loadBooleanValue(Config.GROUP_KEYS));
        wrapLineAtEscapedNewLine.setSelected(Config.loadBooleanValue(Config.WRAP_LINE_AT_ESCAPED_NEW_LINE));
        spacesAroundEqualsSigns.setSelected(Config.loadBooleanValue(Config.SPACES_AROUND_EQUAL_SIGNS));

        groupLevelDepth.setText(Config.loadValue(Config.GROUP_LEVEL_DEPTH));
        keyGroupSeparator.setText(Config.loadValue(Config.KEY_GROUP_SEPARATOR));
    }

    private void initListeners() {
        DocumentListener documentListener = new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                setDirty();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                setDirty();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                setDirty();
            }
        };

        ItemListener itemListener = e -> setDirty();

        groupLevelDepth.getDocument().addDocumentListener(documentListener);
        keyGroupSeparator.getDocument().addDocumentListener(documentListener);

        keepEmptyFields.addItemListener(itemListener);
        convertEncodedToUnicode.addItemListener(itemListener);
        convertUnicodeToEncoded.addItemListener(itemListener);
        convertUnicodeToEncodedUpper.addItemListener(itemListener);
        alignEqualSigns.addItemListener(itemListener);
        groupAlignEqualSigns.addItemListener(itemListener);
        groupKeys.addItemListener(itemListener);
        wrapLineAtEscapedNewLine.addItemListener(itemListener);
        spacesAroundEqualsSigns.addItemListener(itemListener);
    }


    private void setDirty() {
        modified = true;
    }

    private void initComponents() {
        keyGroupSeparator = new JTextField();
        groupLevelDepth = new JTextField();

        convertEncodedToUnicode = new JBCheckBox(Strings.CONF__CONVERT_ENCODED_TO_UNICODE.getDescription());
        convertUnicodeToEncoded = new JBCheckBox(Strings.CONF__CONVERT_UNICODE_TO_ENCODED.getDescription());
        keepEmptyFields = new JBCheckBox(Strings.CONF__KEEP_EMPTY_FIELDS.getDescription());
        groupKeys = new JBCheckBox(Strings.CONF__GROUP_KEYS.getDescription());
        convertUnicodeToEncodedUpper = new JBCheckBox(Strings.CONF__CONVERT_UNICODE_TO_ENCODED_UPPER.getDescription());
        alignEqualSigns = new JBCheckBox(Strings.CONF__ALIGN_EQUAL_SIGNS.getDescription());
        groupAlignEqualSigns = new JBCheckBox(Strings.CONF__GROUP_ALIGN_EQUAL_SIGNS.getDescription());
        wrapLineAtEscapedNewLine = new JBCheckBox(Strings.CONF__WRAP_LINE_AT_ESCAPED_NEW_LINE.getDescription());
        spacesAroundEqualsSigns = new JBCheckBox(Strings.CONF__SPACES_AROUND_EQUAL_SIGNS.getDescription());
    }


    @Override
    public boolean isModified() {
        return modified;
    }

    @Override
    public void apply() throws ConfigurationException {
        Config.saveBooleanValue(Config.KEEP_EMPTY_FIELDS, keepEmptyFields.isSelected());
        Config.saveBooleanValue(Config.CONVERT_ENCODED_TO_UNICODE, convertEncodedToUnicode.isSelected());
        Config.saveBooleanValue(Config.CONVERT_UNICODE_TO_ENCODED, convertUnicodeToEncoded.isSelected());
        Config.saveBooleanValue(Config.CONVERT_UNICODE_TO_ENCODED_UPPER, convertUnicodeToEncodedUpper.isSelected());
        Config.saveBooleanValue(Config.ALIGN_EQUAL_SIGNS, alignEqualSigns.isSelected());
        Config.saveBooleanValue(Config.GROUP_ALIGN_EQUAL_SIGNS, groupAlignEqualSigns.isSelected());
        Config.saveBooleanValue(Config.GROUP_KEYS, groupKeys.isSelected());
        Config.saveBooleanValue(Config.KEEP_EMPTY_FIELDS, keepEmptyFields.isSelected());
        Config.saveBooleanValue(Config.WRAP_LINE_AT_ESCAPED_NEW_LINE, wrapLineAtEscapedNewLine.isSelected());
        Config.saveBooleanValue(Config.SPACES_AROUND_EQUAL_SIGNS, spacesAroundEqualsSigns.isSelected());

        Config.saveValue(Config.GROUP_LEVEL_DEPTH, groupLevelDepth.getText());
        Config.saveValue(Config.KEY_GROUP_SEPARATOR, keyGroupSeparator.getText());

        modified = false;
    }

    @Override
    public void reset() {
        loadAndSetValues();
        modified = false;
    }

    @Override
    public void disposeUIResources() {
    }

}
