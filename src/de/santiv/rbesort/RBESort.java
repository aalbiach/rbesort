package de.santiv.rbesort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.essiembre.eclipse.rbe.model.bundle.Bundle;
import com.essiembre.eclipse.rbe.model.bundle.PropertiesGenerator;
import com.essiembre.eclipse.rbe.model.bundle.PropertiesParser;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.CommandProcessor;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.DocumentRunnable;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;

import de.santiv.rbesort.enums.Config;


public class RBESort extends AnAction {
    public void actionPerformed(AnActionEvent e) {

        final Project project = e.getProject();
        if (project == null) return;

        Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
        if (editor == null) return;

        final Document document = editor.getDocument();
        if (document == null) return;

        VirtualFile virtualFile = FileDocumentManager.getInstance().getFile(document);
        if (Config.loadBooleanValue(Config.FORMAT_ONLY_ALLOWED_FILE_EXTENSIONS) && !getAllowedFileExtensions().contains(virtualFile.getExtension())) return;

        Bundle parse = PropertiesParser.parse(document.getText());
        if (parse == null) return;

        final String generate = PropertiesGenerator.generate(parse);

        ApplicationManager.getApplication().runWriteAction(new DocumentRunnable(document, null) {
            @Override
            public void run() {
                CommandProcessor.getInstance().runUndoTransparentAction(() -> document.setText(generate));
            }
        });
    }

    private List<String> getAllowedFileExtensions() {
        List<String> allowedFileExtensions = new ArrayList<>();
        Collections.addAll(allowedFileExtensions, Config.loadValue(Config.ALLOWED_FILE_EXTENSIONS).split(";"));
        return allowedFileExtensions;
    }
}
